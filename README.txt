# Curator #

This is a mockup for a WordPress theme I'm currently designing.

## Credits ##

* [Fjalla One font](https://www.google.com/fonts/specimen/Fjalla+One) is copyright [Sorkin Type](http://www.sorkintype.com) and is licensed under the [SIL Open Font License, v1.1](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
* [Average font](https://www.google.com/fonts/specimen/Average) is copyright Eduardo Tunni and is licensed under the [SIL Open Font License, v1.1](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
* All bundled images were obtained at [Unsplash](http://unsplash.com) and are licensed under the [CC0](http://creativecommons.org/publicdomain/zero/1.0/)