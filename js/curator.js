jQuery( document ).ready( function( $ ) {
	var menuButton = $( '.menu-toggle' );
	var header = $( '.masthead' );
	
	menuButton.on( 'click', function( e ) {
		e.preventDefault();
		$( 'body' ).toggleClass( 'menu-active' );
	} );
} );
