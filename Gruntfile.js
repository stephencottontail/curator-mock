module.exports = function(grunt) {
	var path = process.cwd();
	var cwd = path.substring( path.lastIndexOf( '/' ) + 1 );
	
	grunt.initConfig( {
		pkg: grunt.file.readJSON( 'package.json' ),
		cwd: cwd,
		copy: {
			main: {
				files: [ 
					{ expand: true, src: 'README.md', rename: function( dest, src ) { return src.replace( '.md', '.txt' ); } }
				]
			},
			prepare: {
				files: [
					{ expand: true, src: [ '**', '!Gruntfile.js', '!package.json', '!sass/**', '!node_modules/**' ], dest: '/Users/stephen/Desktop/<%= cwd %>', filter: 'isFile' }
				]
			}
		},
		sass: {
			dist: {
				options: {
					style: 'compact',
					loadPath: '~/Sites/assets',
				},
				files: {
					'css/style.css' : 'sass/style.scss'
				}
			}
		},
		browserSync: {
			default_options: {
				bsFiles: {
					src: [ 'css/*.css', '*.php', '*.html', '**/*.js' ]
				},
				options: {
					watchTask: true,
					proxy: 'el-capitan.local'
				}
			}
		},
		postcss: {
			options: {
      	map: true,
				processors: [
					require( 'autoprefixer' )( {
						browsers: [ '> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1' ]
        	} )
				]
    	},
    	dist: {
	    	src: 'css/*.css'
    	}
		},
		watch: {
			css: {
				files: '**/*.scss',
				tasks: [ 'sass', 'postcss' ]
			},
			readme: {
				files: 'README.md',
				tasks: [ 'copy:main' ]
			}
		}
	} );
	grunt.loadNpmTasks( 'grunt-postcss' );
	grunt.loadNpmTasks( 'grunt-contrib-sass' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-browser-sync' );
	grunt.loadNpmTasks( 'grunt-contrib-copy' );
	grunt.registerTask( 'default', [ 'copy:main', 'browserSync', 'watch', 'postcss' ] );
	grunt.registerTask( 'prepare', [ 'postcss', 'copy:prepare' ] );
}
